package org.leadium.core.utils

import java.math.RoundingMode
import java.text.DecimalFormat

fun decimalFormat(amount: Double, roundingMode: RoundingMode, pattern: String): String {
    val decimalFormat = DecimalFormat(pattern)
    decimalFormat.roundingMode = roundingMode
    return decimalFormat.format(amount).toString()
}