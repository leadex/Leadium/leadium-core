package org.leadium.core.utils

/**
 * @suppress
 */
fun LinkedHashMap<Any, Any>.subMap(fromIndex: Int, toIndex: Int): LinkedHashMap<Any, Any> {
    val target = LinkedHashMap<Any, Any>()
    val subList = this.toList().subList(fromIndex, toIndex)
    subList.forEach { pair: Pair<Any, Any>? -> target[pair!!.first] = pair.second }
    return target
}