package org.leadium.core.utils

import org.json.JSONException
import org.json.JSONObject

/**
 * Method tries to get Any from the given JSONObject
 */
fun JSONObject.tryToGetAny(key: String): Any? {
    return try {
        this.get(key)
    } catch (e: JSONException) {
        null
    }
}

/**
 * Method tries to get String from the given JSONObject
 */
fun JSONObject.tryToGetString(key: String): String? {
    return try {
        this.getString(key)
    } catch (e: JSONException) {
        null
    }
}