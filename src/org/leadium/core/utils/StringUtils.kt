package org.leadium.core.utils

import java.util.regex.Pattern

/**
 * Method returns a random string with the specified length.
 * @param length Int
 * @return String
 */
fun randomString(length: Int = 3): String {
    val allowedChars = ('a'..'z')
    return (1..length)
        .map { allowedChars.random() }
        .joinToString("")
}

/**
 * Method replaces camel cases of the given string with spaces
 */
fun String.replaceCamelCaseWithSpaces() =
    this.replace(Regex("([A-Z])([A-Z])([a-z])|([a-z])([A-Z])"), "\$1\$4 \$2\$3\$5")
        .replace(Regex("([0-9])([A-Z])([a-z])|([a-z])([0-9])"), "\$1\$4 \$2\$3\$5")
        .replace(Regex("([0-9])([A-Z])([a-z])"), "\$1 \$2\$3")

/**
 * Method replaces camel cases of the given string with underscores
 */
fun String.replaceCamelCaseWithUnderscores() =
    this.replace(Regex("([A-Z])([A-Z])([a-z])|([a-z])([A-Z])"), "\$1\$4_\$2\$3\$5")

fun String.toList(delimiter: String = "; ") = try {
    this.split(delimiter).toList()
} catch (e: NullPointerException) {
    emptyList()
}

fun extractByRegex(regex: String, source: String): String {
    val preparedSource: String = source
    val p = Pattern.compile(regex)
    val m = p.matcher(preparedSource)
    while (m.find()) {
        return m.group()
    }
    throw NullPointerException("Source: $preparedSource, RegEx: $regex")
}