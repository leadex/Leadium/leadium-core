package org.leadium.core.utils

/**
 * Method returns a random number as a integer in the specified range.
 * @param min Int
 * @param max Int
 * @return Int
 */
fun randomInt(min: Int, max: Int) = (Math.random() * (max - min)).toInt() + min

/**
 * Method returns a random number as a long in the specified range.
 * @param min Long
 * @param max Long
 * @return Long
 */
fun randomLong(min: Long, max: Long) = (Math.random() * (max - min)).toLong() + min

/**
 * Method returns a random number as a float in the specified range.
 * @param min Float
 * @param max Float
 * @return Float
 */
fun randomFloat(min: Float, max: Float) = (Math.random() * (max - min)).toFloat() + min