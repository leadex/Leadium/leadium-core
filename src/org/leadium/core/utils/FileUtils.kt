package org.leadium.core.utils

import java.io.File

/**
 * Method creates new file if it not exist
 */
fun createNewFile(file: File): File {
    with(file) {
        if (!exists()) createNewFile()
        return this
    }
}