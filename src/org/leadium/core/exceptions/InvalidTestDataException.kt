package org.leadium.core.exceptions

class InvalidTestDataException(message: String) : Exception(message)