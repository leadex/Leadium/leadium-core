package org.leadium.core

operator fun <S : Any, R> S.minus(block: S.() -> R): R = block.invoke(this)

