package org.leadium.core

/**
 * The interface uses for test-cases.
 */
interface TestCase {

    /**
     * The method executes test-case
     */
    fun begin()
}
