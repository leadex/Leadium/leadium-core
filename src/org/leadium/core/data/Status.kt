package org.leadium.core.data

/**
 * @suppress
 */
enum class Status(val string: String) {
    FAILED("failed"),
    BROKEN("broken"),
    PASSED("passed"),
    TIMEOUT("timeout"),
    SKIPPED("skipped");
}