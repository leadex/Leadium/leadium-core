package org.leadium.core.data

import org.apache.commons.lang3.tuple.Pair

typealias TestDataMap = LinkedHashMap<String, Pair<String, String>>

interface TestData {

    val td: TestDataMap

    operator fun TestDataMap.get(key: StringEnum, index: Int = 0) =
        td.getValidValue(key.string).split(";")[index].trim()

    fun TestDataMap.getValidValue(key: String): String {
        val value: String
        try {
            value = this[key]!!.left!!
        } catch (e: NullPointerException) {
            throw NullPointerException("Value: $key isn't exists in the test data table!")
        }
        return value.ifEmpty { throw NullPointerException("Value: $key is empty in the test data table!") }
    }
}