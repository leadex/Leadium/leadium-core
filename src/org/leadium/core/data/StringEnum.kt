package org.leadium.core.data

import java.util.regex.Pattern

interface StringEnum {

    val name: String
    var string: String

    fun with(vararg parameters: String): StringEnum {
        val pattern = Pattern.compile("(\\{)(.+?)(})")
        val matcher = pattern.matcher(this.string)
        while (matcher.find()) {
            val index = matcher.group().substringAfter("{").substringBefore("}").toInt()
            this.string = string.replace(matcher.group(), parameters[index])
        }
        return this
    }
}