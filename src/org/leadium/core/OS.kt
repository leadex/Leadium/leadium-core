package org.leadium.core

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import java.io.InputStream

object OS {

    suspend fun executeCommand(commandArgs: Array<String>): ExecuteCommandResult {
        return try {
            val process = ProcessBuilder(*commandArgs).start()
            val outputStream = GlobalScope.async(Dispatchers.IO) { readStream(process.inputStream) }
            val errorStream = GlobalScope.async(Dispatchers.IO) { readStream(process.errorStream) }
            val exitCode = withContext(Dispatchers.IO) {
                process.waitFor()
            }
            ExecuteCommandResult(exitCode, outputStream.await(), errorStream.await())
        } catch (e: Exception) {
            ExecuteCommandResult(-1, "", e.localizedMessage)
        }
    }

    private suspend fun readStream(inputStream: InputStream): String {
        val readLines = mutableListOf<String>()
        withContext(Dispatchers.IO) {
            try {
                inputStream.bufferedReader().use { reader ->
                    var line: String?
                    do {
                        line = reader.readLine()
                        if (line != null) {
                            readLines.add(line)
                        }
                    } while (line != null)
                }
            } catch (e: Exception) {
                // ..
            }
        }
        return readLines.joinToString(System.lineSeparator())
    }

    data class ExecuteCommandResult(val exitCode: Int, val outputStream: String, val errorStream: String)
}